<?php

require_once './conf/config.php';

if (isset($_REQUEST)) {
    $montant = $_REQUEST["montant"];
    $date = $_REQUEST["date"];
    $libelle = $_REQUEST["libelle"];
    $visiteurCourant = $_SESSION["connectedUser"];
    if (isset($_SESSION["ficheFraisCourante"])) {
        $ficheFraisCourante = $_SESSION["ficheFraisCourante"];
    } else {
        $moisAnnee = date("mY");
        $ficheFraisCourante = $visiteurCourant->getFicheFrais($moisAnnee);
    }
    try {
        $dateTime = new DateTime($date);
    } catch (Exception $e) {
        $dateTime = new DateTime();
    }
    $ficheFraisCourante->setLigneFraisHorsForfait($libelle, $dateTime, $montant);
    $ficheFraisCourante->save();
    header("Location:visiteur.saisieHorsForfait.php");
}




