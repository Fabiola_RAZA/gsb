<?php

require_once './conf/config.php';

if (isset($_REQUEST)) {
    $idFicheFrais = $_REQUEST["idFicheFrais"];
    $ficheFrais = FicheFrais::fetch($idFicheFrais);
    foreach ($_REQUEST as $idFrais => $quantite) {
        if ($idFrais != "idFicheFrais") {
            $ligneFraisForfait = $ficheFrais->getLigneFraisForfait($idFrais);
            $ligneFraisForfait->setQuantite($quantite);
        }
    }

    $ficheFrais->save();
}

header("Location:comptable.gestionFicheFrais.php");

