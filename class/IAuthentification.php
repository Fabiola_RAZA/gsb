<?php

/**
 * Application <Appli-Frais>
 */

/**
 * Interface d'autentification
 * 
 * @author pvig <vignard.philippe@laposte.net>
 * @package GSB
 * @version 1.0
 * @category Technical Interface
 * 
 */
interface IAuthentification {
    public function check($login,$password,$options=null);
}
