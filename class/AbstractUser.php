<?php

/**
 * Application <Appli-Frais>
 */

/**
 * Représente un utilisateur du système
 * 
 * @author pvig <vignard.philippe@laposte.net>
 * @package GSB
 * @version 1.0.0
 * @category Domain class
 * 
 */
abstract class AbstractUser {

    /**
     * Obtient l'identifiant de l'utilisateur
     * 
     * @return string l'identifiant de l'utilisateur
     */
    public function getIdUser() {
        return $this->idUser;
    }

    /**
     * Obtient le login de l'utilisateur
     * 
     * @return string le login de l'utilisateur
     */
    public function getLogin() {
        return $this->login;
    }

    /**
     * Obtient le mot de passe de l'utilisateur
     * 
     * @return string
     */
    public function getMdp() {
        return $this->mdp;
    }

    /**
     * Définit le login de l'utilisateur
     * 
     * @param string $login le login de l'utilisateur
     */
    public function setLogin($login) {
        $this->login = $login;
    }

    /**
     * Définit le mot de passe de l'utilisateur
     * 
     * @param string $mdp
     */
    public function setMdp($mdp) {
        $this->mdp = $mdp;
    }

    /**
     * Obtient le nom de l'utilisateur
     * 
     * @return string le nom de l'utilisateur
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Obtient le prénom de l'utilisateur
     * 
     * @return string le prénom de l'utilisateur
     */
    public function getPrenom() {
        return $this->prenom;
    }

    /**
     * Obtient l'adresse de l'utilisateur
     * 
     * @return string l'adresse de l'utilisateur
     */
    public function getAdresse() {
        return $this->adresse;
    }

    /**
     * Obtient la ville de l'utilisateur
     * 
     * @return string la ville de l'utilisateur
     */
    public function getVille() {
        return $this->ville;
    }

    /**
     * Obtient le code postal de l'utilisateur
     * 
     * @return string le code postal de l'utilisateur
     */
    public function getCp() {
        return $this->cp;
    }

    /**
     * Obtient la date d'embauche de l'utilisateur
     * 
     * @return string la date d'embauche de l'utilisateur
     */
    public function getDateEmbauche() {
        return $this->dateEmbauche;
    }

    /**
     * Définit le nom de l'utilisateur
     * 
     * @param string $nom le nom de l'utilisateur
     */
    public function setNom($nom) {
        $this->nom = $nom;
    }

    /**
     * Définit le prénom de l'utilisateur
     * 
     * @param string $prenom le prénom de l'utilisateur
     */
    public function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    /**
     * Définit l'adresse de l'utilisateur
     * 
     * @param string $adresse l'adresse de l'utilisateur
     */
    public function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    /**
     * Définit la ville de l'utilisateur
     * 
     * @param string $ville la ville de l'utilisateur
     */
    public function setVille($ville) {
        $this->ville = $ville;
    }

    /**
     * Définit le code postal de l'utilisateur
     * 
     * @param string $cp le code postal de l'utilisateur
     */
    public function setCp($cp) {
        $this->cp = $cp;
    }

    /**
     * Définit la date d'embauche de l'utilisateur
     * 
     * @param string $dateEmbauche la date d'embauchede l'utilisateur
     */
    public function setDateEmbauche($dateEmbauche) {
        $this->dateEmbauche = $dateEmbauche;
    }

    protected $idUser;
    protected $nom;
    protected $prenom;
    protected $adresse;
    protected $ville;
    protected $cp;
    protected $dateEmbauche;
    protected $login;
    protected $mdp;
    protected static $select = 'select * from user';
    protected static $selectById = 'select * from user where idUser = :idUser ';
    protected static $insert = 'insert into user (nom,prenom,adresse,ville,cp,dateEmbauche,login,mdp,profil) values (:nom,:prenom,:adresse,:ville,:cp,:dateEmbauche,:login,:mdp,:profil)';
    protected static $udpate = 'update user set nom=:nom,prenom=:prenom,adresse=:adresse,ville=:ville,cp=:cp,dateEmbauche=:dateEmbauche,login=:login,mdp=:mdp,profil=:profil where idUser=:idUser';
    protected static $delete = 'delete from user where idUser=:idUser on delete constraint';

}
