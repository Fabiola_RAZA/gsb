<?php

require_once './conf/config.php';

if (isset($_REQUEST) && isset($_SESSION["ficheFraisCourante"])) {
       $ficheFraisCourante = $_SESSION["ficheFraisCourante"] ;
    foreach ($_REQUEST as $idFrais => $quantite) {
        if ($idFrais != "nbJustificatifs"){
           $ligneFraisForfait = $ficheFraisCourante->getLigneFraisForfait($idFrais);
           $ligneFraisForfait->setQuantite($quantite); 
        }
    }
    $ficheFraisCourante->save();
}

$nbJustificatifs = $_REQUEST["nbJustificatifs"];
$ficheFraisCourante -> getNbJustificatifs($nbJustificatifs);

header("Location: visiteur.saisieFicheFrais.php");

